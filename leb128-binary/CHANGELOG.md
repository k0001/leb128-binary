# Version 0.1.3

* Add to `ULEB128`: `putText`, `getText`, `putLazyText`, `getLazyText`,
  `putList`, `getList`, `putSeq`, `getSeq`, `putSet`, `getSet`, `putMap`,
  `getMap`.

* Add to `SLEB128`: `putScientific`, `getScientific`, `putRational`,
  `getRational`, `putFixed`, `getFixed`.


# Version 0.1.2

* Faster `ULEB128` and `SLEB128` encoding and decoding.

* Added `ZLEB128`. That is, ZigZag encoding of signed numbers in combination
  with `ULEB128`.

* Added benchmarks.


# Version 0.1.1

* COMPILER ASSISTED BREAKING CHANGE on `ULEB128`: `getNatural` takes an maximum
  number of bytes to process as input. See issue #1.

* COMPILER ASSISTED BREAKING CHANGE on `SLEB128`: `getInteger` takes an maximum
  number of bytes to process as input. See issue #1.

* Added on `ULEB128`: `putByteString`, `getByteString`, `putShortByteString`,
  `getShortByteString`, `putLazyByteString`, `getLazyByteString`, `getIntegral`,
  `getInt`, `getInt8`, `getInt16`, `getInt32`, `getInt64`.

* Added on `ULEB128`: `putNatural`, `putWord`, `putWord8`, `putWord16`,
  `putWord32`, `putWord64`, `getNatural`, `getWord`, `getWord8`, `getWord16`,
  `getWord32`, `getWord64`.


# Version 0.1

* Initial version
