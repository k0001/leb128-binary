{ mkDerivation, base, binary, bytestring, containers, deepseq
, hedgehog, integer-logarithms, lib, scientific, tasty, tasty-bench
, tasty-hedgehog, tasty-hunit, text
}:
mkDerivation {
  pname = "leb128-binary";
  version = "0.1.3";
  src = ./.;
  libraryHaskellDepends = [
    base binary bytestring containers integer-logarithms scientific
    text
  ];
  testHaskellDepends = [
    base binary bytestring containers hedgehog integer-logarithms
    scientific tasty tasty-hedgehog tasty-hunit text
  ];
  benchmarkHaskellDepends = [
    base binary bytestring containers deepseq integer-logarithms
    scientific tasty-bench text
  ];
  homepage = "https://gitlab.com/k0001/leb128-binary";
  description = "Signed and unsigned LEB128 codec for binary library";
  license = lib.licenses.asl20;
}
