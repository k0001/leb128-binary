# leb128-binary

Signed and unsigned [LEB128](https://en.wikipedia.org/wiki/LEB128) codec 
for Haskell's [binary](https://hackage.haskell.org/package/binary) library.


### License

Apache-2.0 (see `LICENSE` file).
