{
  description = "Haskell 'leb128-binary' library";

  inputs = {
    flakety.url = "github:k0001/flakety";
    nixpkgs.follows = "flakety/nixpkgs";
    flake-parts.follows = "flakety/flake-parts";
  };

  outputs = inputs@{ ... }:
    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      flake.overlays.default = inputs.nixpkgs.lib.composeManyExtensions [
        inputs.flakety.overlays.default
        (final: prev:
          let
            hsLib = prev.haskell.lib;
            hsClean = drv:
              hsLib.overrideCabal drv
              (old: { src = prev.lib.sources.cleanSource old.src; });
          in {
            haskell = prev.haskell // {
              packageOverrides = prev.lib.composeExtensions
                (prev.haskell.packageOverrides or (_: _: { })) (hself: hsuper: {
                  leb128-binary =
                    hsClean (hself.callPackage ./leb128-binary { });
                });
            };
          })
      ];
      systems = [ "x86_64-linux" "i686-linux" "aarch64-linux" ];
      perSystem = { config, pkgs, system, ... }: {
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;
          overlays = [ inputs.self.overlays.default ];
        };
        packages = {
          leb128-binary__ghc96 = pkgs.haskell.packages.ghc96.leb128-binary;
          leb128-binary__ghc98 = pkgs.haskell.packages.ghc98.leb128-binary;

          leb128-binary__sdist =
            pkgs.haskellPackages.cabalSdist { src = ./leb128-binary; };
          leb128-binary__sdistDoc = pkgs.haskell.lib.documentationTarball
            config.packages.leb128-binary__ghc96;

          default = pkgs.releaseTools.aggregate {
            name = "every output from this flake";
            constituents = [
              config.packages.leb128-binary__ghc96
              config.packages.leb128-binary__ghc96.doc
              config.devShells.ghc96

              config.packages.leb128-binary__ghc98
              config.packages.leb128-binary__ghc98.doc
              config.devShells.ghc98

              config.packages.leb128-binary__sdist
              config.packages.leb128-binary__sdistDoc

            ];
          };
        };
        devShells = let
          hsLib = pkgs.haskell.lib;
          mkShellFor = ghc:
            ghc.shellFor {
              packages = p: [ (hsLib.doBenchmark p.leb128-binary) ];
              doBenchmark = true;
              withHoogle = true;
              nativeBuildInputs =
                [ pkgs.cabal-install pkgs.cabal2nix pkgs.ghcid ];
            };
        in {
          default = config.devShells.ghc98;
          ghc96 = mkShellFor pkgs.haskell.packages.ghc96;
          ghc98 = mkShellFor pkgs.haskell.packages.ghc98;
        };
      };
    };
}
